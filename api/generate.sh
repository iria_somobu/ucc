#!/bin/bash

cd "$(dirname "$0")" || exit

echo "Generating accounting api"
./generator/generator.py -t generator/ -d ../ -s accounting.md -p ucc.accounting --methods=Accounting --events=AccountEvents

echo "Generating adapter api"
./generator/generator.py -t generator/ -d ../ -s adapter.md -p ucc.adapter --methods=Adapter --events=AdapterEvents

echo "! Do not forget to rebuild android project"
echo "Done"
