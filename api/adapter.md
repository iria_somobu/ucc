# objects

# methods

## setRealtime

Changes realtime state of adapter between offline and realtime:

- **offline mode**: adapters works in a way that makes current user indistinctible from being offline: user's profile
  badge says 'offline', no messages marked as read, etc. Any action revealing user's real presence should be ignored in
  this state, user may be notified that action is not supported with corresponding `onWarning` event;
- **realtime mode**: normal, fully-featured working mode.

| Name       | Type | Desc |
|------------|------|------|
| isRealtime | bool |      |

# events

## onWarning

Notify user about something error-alike, but not the error.

| Name    | Type | Desc                                         |
|---------|------|----------------------------------------------|
| title   | str  | Warning title, single line, about 20 symbols |
| message | str  | Warning message, about 80 symbols            |