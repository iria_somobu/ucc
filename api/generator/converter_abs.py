import io
import os
import re

verbose = False

STATE_IGNORE = 0  # Ignore everything until next #-block
STATE_COPY = 1    # Copy everything until next #-block
STATE_SIGNATURE = 2
STATE_SIG_TABLE_HEADER = 3
STATE_SIG_TABLE_CONTENTS = 4

OUTPUT_OBJECTS = 0
OUTPUT_METHODS = 1
OUTPUT_EVENTS = 2

FIELD_FLAG_ARRAY = 'a'


class MaSignature:
    def __init__(self):
        self.name = ""
        self.descr = ""
        self.field_names = []  # Strings
        self.field_types = []  # Strings
        self.field_descs = []  # Strings
        self.field_flags = []  # String with chars representing flag
        self.append = ""


lines = []
objects = []
methods = []
events = []




def parse_file(filename):
    state = STATE_IGNORE
    output = OUTPUT_METHODS
    current_sig = MaSignature()

    with open(filename, encoding="utf-8") as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]

    for line in lines:
        line = line.strip(' ')

        # TODO: methods/callback check
        if line.startswith('#'):
            line = line[1:].strip(' ')

            if current_sig.name:
                if output == OUTPUT_OBJECTS:
                    objects.append(current_sig)
                elif output == OUTPUT_METHODS:
                    methods.append(current_sig)
                elif output == OUTPUT_EVENTS:
                    events.append(current_sig)

            current_sig = MaSignature()

            if line == "objects":
                state = STATE_IGNORE
                output = OUTPUT_OBJECTS
                if verbose: print("Appending objects")
            if line == "methods":
                state = STATE_IGNORE
                output = OUTPUT_METHODS
                if verbose: print("Appending methods")
            if line == "events":
                state = STATE_IGNORE
                output = OUTPUT_EVENTS
                if verbose: print("Appending events")
            elif line.startswith('#'):
                line = line[1:].strip(' ')
                state = STATE_SIGNATURE
                if verbose: print("Found record:", line)

                current_sig.name = line
            else:
                state = STATE_IGNORE

        else:
            if state == STATE_IGNORE:
                continue
            elif state == STATE_COPY:
                print('TODO: copy', line)
            elif state == STATE_SIGNATURE:
                if line.replace(' ', '') == '|Name|Type|Desc|':
                    state = STATE_SIG_TABLE_HEADER
                else:
                    current_sig.descr += line + "\n"
            elif state == STATE_SIG_TABLE_HEADER:
                if re.match(r"^\|\s*-+\s*\|\s*-+\s*\|\s*-+\s*\|$", line):
                    state = STATE_SIG_TABLE_CONTENTS
                else:
                    print('Malformed subheader dude')
            elif state == STATE_SIG_TABLE_CONTENTS:
                if line.startswith('|'):
                    parts = line.split('|')
                    if len(parts) != 5:
                        print('Malformed line dude: ', line)
                    else:
                        current_sig.field_names.append(parts[1].strip(' '))

                        cur_field_type = parts[2].strip(' ')
                        cur_field_flags = ""

                        if "[]" in cur_field_type:
                            cur_field_flags += FIELD_FLAG_ARRAY
                            cur_field_type = cur_field_type.replace('[]', '')

                        current_sig.field_types.append(cur_field_type)
                        current_sig.field_flags.append(cur_field_flags)
                        current_sig.field_descs.append(parts[3].strip(' '))
                else:
                    state = STATE_SIGNATURE
                    current_sig.descr += line + "\n"

            else:
                print('Dunno dude ', state, " ", line)

    if current_sig.name:
        if output == OUTPUT_OBJECTS:
            objects.append(current_sig)
        elif output == OUTPUT_METHODS:
            methods.append(current_sig)
        elif output == OUTPUT_EVENTS:
            events.append(current_sig)