#!/usr/bin/python3

import argparse
import os

import converter_abs as conv

# Parse cmdline arguments
parser = argparse.ArgumentParser(description='Bindings generator for Java')
parser.add_argument('--events', action='store', default='Events', help='events object name')
parser.add_argument('--methods', action='store', default='Methods', help='methods object name')
parser.add_argument('-d', action='store', default='.', help='destination directory')
parser.add_argument('-p', action='store', default='ucc.api', help='package name')
parser.add_argument('-s', action='store', default='api/gen/api.md', help='source .md file')
parser.add_argument('-t', action='store', default='.', help='templates directory')
parser.add_argument('-v', action='store_true', help='verbose mode')

args = parser.parse_args()

events = args.events
methods = args.methods
verbose = args.v
package = args.p
source = args.s
tpl = args.t
dst = args.d

# Conversion maps

jClassMappings = {
    "bool": "boolean",
    "str": "String",
}

parcelInMappings = {
    "str": "in.readString();",
    "str[]": "in.readStringArray();",
    "bool": "in.readBoolean();",
    "bool[]": "in.readBooleanArray();"
}

parcelOutMappings = {
    "str": "parcel.writeString(FIELD);",
    "str[]": "parcel.writeStringArray(FIELD);",
    "bool": "parcel.writeBoolean(FIELD);",
    "bool[]": "parcel.writeBooleanArray(FIELD);"
}

templateKeywordsMappings = {
    'ucc.api': package,
    'MethodsObject': methods,
    'IEvents': f"I{events}",
    'EventsObject': events
}


# Functions

def get_full_type(type, flags):
    arrayMark = ''
    if conv.FIELD_FLAG_ARRAY in flags:
        arrayMark = "[]"

    return jClassMappings.get(type, type) + arrayMark


def get_aidl_full_type(type, flags):
    arrayMark = ''
    prefix = ''
    if conv.FIELD_FLAG_ARRAY in flags:
        arrayMark = "[]"
        prefix = 'inout '

    return prefix + jClassMappings.get(type, type) + arrayMark


def get_return_type(obj):
    return_type = "void"
    for i in range(len(obj.field_names)):
        if obj.field_names[i] == "@return":
            return_type = get_full_type(obj.field_types[i], obj.field_flags[i])
    return return_type


def get_javadoc(obj):
    spacer = '    '
    result = '\n' + spacer + '/**\n'
    for dc in obj.descr.split('\n'):
        dc = dc.strip(' ')
        if len(dc) != 0:
            result += spacer + " * " + dc + '\n'

    result += spacer + ' * \n'

    for i in range(len(obj.field_names)):
        if obj.field_names[i] == "@return":
            result += spacer + ' * ' + obj.field_names[i] + ' ' + obj.field_descs[i] + '\n'
        else:
            result += spacer + ' * @param ' + obj.field_names[i] + ' ' + obj.field_descs[i] + '\n'
    result += spacer + ' */\n'
    return result


def get_obj_map(obj):  # Object template replacement map
    result = {
        'ucc.api': package,
        "OBJECT_NAME": obj.name
    }

    spacer = '    '

    descr = '/**\n'
    for dc in obj.descr.split('\n'):
        dc = dc.strip(' ')
        if len(dc) != 0:
            descr += (" * " + dc + '\n')
    descr += ' */'
    result["/* Generator: descr */"] = descr

    fields = ''
    for i in range(len(obj.field_names)):
        fields += '\n' + spacer + '/**\n'
        for dc in obj.field_descs[i].split('\n'):
            dc = dc.strip(' ')
            if len(dc) != 0:
                fields += (spacer + " * " + dc + '\n')
        fields += (spacer + ' */\n')

        mapped_type = get_full_type(obj.field_types[i], obj.field_flags[i])
        fields += (spacer + 'public ' + mapped_type + ' ' + obj.field_names[i] + ' = null;\n')
    result["/* Generator: fields */"] = fields

    parcelIn = ''
    for i in range(len(obj.field_names)):
        expr = ""
        if obj.field_types[i] in parcelInMappings:
            expr += parcelInMappings.get(obj.field_types[i])
        elif "a" in obj.field_flags[i]:
            expr += "in.readParcelableArray(getClass().getClassLoader());"
        else:
            expr += "in.readParcelable(getClass().getClassLoader());"
        parcelIn += f"{spacer * 2}{obj.field_names[i]} = {expr}\n"
    result["/* Generator: parcel in */"] = parcelIn

    parcelOut = ''
    for i in range(len(obj.field_names)):
        expr = ""
        if obj.field_types[i] in parcelOutMappings:
            expr += parcelOutMappings.get(obj.field_types[i])
        elif "a" in obj.field_flags[i]:
            expr += "parcel.writeParcelableArray(FIELD, PARCELABLE_WRITE_RETURN_VALUE);"
        else:
            expr += "parcel.writeParcelable(FIELD, PARCELABLE_WRITE_RETURN_VALUE);"
        expr = expr.replace("FIELD", obj.field_names[i])
        parcelOut += spacer + spacer + expr + "\n"
    result["/* Generator: parcel out */"] = parcelOut

    return result


def get_intf_methods(objs, with_impl=True, with_droid=False):
    result = ""

    for obj in objs:

        return_type = get_return_type(obj)
        prefix = 'public ' if with_impl else ''
        result += (get_javadoc(obj))
        if with_droid: result += '    @Override\n'
        result += ('    ' + prefix + return_type + " " + obj.name + "(")

        for i in range(len(obj.field_names)):
            if obj.field_names[i] != "@return":
                if i > 0:
                    result += ', '

                type = get_full_type(obj.field_types[i], obj.field_flags[i])
                result += (type + ' ' + obj.field_names[i])

        result += ")"

        if with_droid:
            result += " throws RemoteException"

        if with_impl:
            result += """ {
        throw new RuntimeException("Unimplemented");
    }
        """
        else:
            result += ";"

    return result


def get_aidl_methods(objs):
    result = ""

    for obj in objs:

        return_type = get_return_type(obj)
        result += (get_javadoc(obj))
        result += ('    ' + return_type + " " + obj.name + "(")

        for i in range(len(obj.field_names)):
            if obj.field_names[i] != "@return":
                if i > 0:
                    result += ', '

                type = get_aidl_full_type(obj.field_types[i], obj.field_flags[i])
                result += (type + ' ' + obj.field_names[i])

        result += ");\n"

    return result


def get_intf_parcelables(objs):
    rz_set = set()
    rz_exclude = {'void', 'str', 'bool'}

    for obj in objs:
        for i in range(len(obj.field_names)):
            rz_set.add(obj.field_types[i])

    rz_set = rz_set - rz_exclude

    rz = ""
    for entry in rz_set:
        rz += "parcelable " + entry + ";\n"

    return rz


# Parse api description that'll be put into four global arrays: lines, objects, methods, events
conv.parse_file(source)

if verbose: print("Generating output...")

# Generate default api
data_j = f'{tpl}/java'
destination = f'{dst}/api/java/src/' + package.replace('.', '/') + '/'
if verbose: print("Writing into folder", destination)
if not os.path.exists(destination): os.makedirs(destination)

with open(f'{data_j}/Object.proto.java', 'r') as input:
    data = input.read()

    for obj in conv.objects:
        obj_map = get_obj_map(obj)
        formatted = data + ""
        for key in obj_map.keys():
            formatted = formatted.replace(key, obj_map[key])

        with open(destination + "" + obj.name + ".java", 'w+') as output:
            output.write(formatted)

with open(f'{data_j}/MethodsObject.proto.java', 'r') as input:
    data = input.read()

    for key in templateKeywordsMappings:
        data = data.replace(key, templateKeywordsMappings[key])

    data = data.replace("/* Generator: inheritance */", "")
    data = data.replace("/* Generator: additional imports */", "// No additional imports here")
    data = data.replace("/* Generator: methods */", get_intf_methods(conv.methods))
    with open(f"{destination}/{methods}.java", 'w+') as output:
        output.write(data)

with open(f'{data_j}/IEvents.proto.java', 'r') as input:
    data = input.read()

    for key in templateKeywordsMappings:
        data = data.replace(key, templateKeywordsMappings[key])

    data = data.replace("/* Generator: methods */", get_intf_methods(conv.events, False))
    with open(f"{destination}/I{events}.java", 'w+') as output:
        output.write(data)

# Generate android api
data_a = f'{tpl}/android'
destination_a = f'{dst}/api/android/src/main/aidl/' + package.replace('.', '/') + '/'
destination_j = f'{dst}/api/android/src/main/java/' + package.replace('.', '/') + '/'
if verbose: print("Writing into folder", destination_a)

if not os.path.exists(destination_a): os.makedirs(destination_a)
if not os.path.exists(destination_j): os.makedirs(destination_j)

with open(f'{data_a}/Object.proto.java', 'r') as input:
    data = input.read()

    for obj in conv.objects:
        obj_map = get_obj_map(obj)
        formatted = data + ""
        for key in obj_map.keys():
            formatted = formatted.replace(key, obj_map[key])

        with open(f"{destination_j}/{obj.name}.java", 'w+') as output:
            output.write(formatted)

with open(f'{data_a}/IMethodsObject.proto.aidl', 'r') as input:
    data = input.read()

    for key in templateKeywordsMappings:
        data = data.replace(key, templateKeywordsMappings[key])

    data = data.replace("/* Generator: additional imports */", f"import {package}.I{events};")
    data = data.replace("/* Generator: parcelables */", get_intf_parcelables(conv.methods))
    data = data.replace("/* Generator: methods */", get_aidl_methods(conv.methods))
    with open(f"{destination_a}/I{methods}.aidl", 'w+') as output:
        output.write(data)

with open(f'{data_a}/IEvents.proto.aidl', 'r') as input:
    data = input.read()

    for key in templateKeywordsMappings:
        data = data.replace(key, templateKeywordsMappings[key])

    data = data.replace("/* Generator: methods */", get_aidl_methods(conv.events))
    with open(f"{destination_a}/I{events}.aidl", 'w+') as output:
        output.write(data)

with open(f'{data_j}/MethodsObject.proto.java', 'r') as input:
    data = input.read()

    for key in templateKeywordsMappings:
        data = data.replace(key, templateKeywordsMappings[key])

    data = data.replace("/* Generator: additional imports */", "import android.os.RemoteException;")
    data = data.replace("/* Generator: inheritance */", f"extends I{methods}.Stub")
    data = data.replace("/* Generator: methods */", get_intf_methods(conv.methods, True, True))
    with open(f"{destination_j}/{methods}.java", 'w+') as output:
        output.write(data)

with open(f'{data_a}/EventsObject.proto.java', 'r') as input:
    data = input.read()

    for key in templateKeywordsMappings:
        data = data.replace(key, templateKeywordsMappings[key])

    data = data.replace("/* Generator: methods */", get_intf_methods(conv.events, True, True))
    with open(f"{destination_j}/{events}.java", 'w+') as output:
        output.write(data)

if verbose: print("Done!")
