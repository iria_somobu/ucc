package ucc.api;

/* Generator: additional imports */

import java.io.File;
import java.lang.reflect.Proxy;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MethodsObject /* Generator: inheritance */ {

    /*
     * Host api
     *
     * - Used by host;
     * - Useless for adapter;
     * - Just let it be;
     */
    private static String workdirWorkaround = "./";
    private final List<IEvents> callbacks = new ArrayList<>();

    public final void addCallback(IEvents callback) {
        synchronized (callbacks) {
            callbacks.add(callback);
        }
    }

    public final void removeCallback(IEvents callback) {
        synchronized (callbacks) {
            callbacks.remove(callback);
        }
    }

    /**
     * On android, we have some troubles with working directory.
     * As workaround, we'll override workdir here.
     */
    public static void setWorkdir(String dir) {
        workdirWorkaround = dir;
    }

    public static String getWorkdir() {
        return workdirWorkaround;
    }


    /*
     * Adapter api
     *
     * Adapter implementation can call or override this methods
     */


    /**
     * Lifecycle method, override me
     * <p>
     * Client(s) will use this adapter soon.
     * </p><p>
     * Adapter should allocate its resources here.
     * </p>
     */
    public void onCreate() {

    }

    /**
     * Lifecycle method, override me
     * <p>
     * Client(s) won't use our adapter anymore.
     * </p><p>
     * Adapter should close all its connections and clear its resources.
     * </p>
     */
    public void onDestroy() {

    }


    private final IEvents cbProxy = (IEvents) Proxy.newProxyInstance(
            MethodsObject.class.getClassLoader(),
            new Class[]{IEvents.class},
            (o, method, objects) -> {
                synchronized (callbacks) {
                    for (IEvents cb : callbacks) {
                        method.invoke(cb, objects);
                    }
                }

                return null;
            }
    );

    /**
     * Using this method adapter implementation can 'broadcast' IEvents' method to all clients
     *
     * @return callback object
     */
    public final IEvents callback() {
        return cbProxy;
    }

    private final Object connectionMonitor = new Object();
    private Connection connection = null;

    /**
     * Opens connection to adapter-local database
     *
     * @return -
     * @throws SQLException -
     */
    public Connection database() throws SQLException {
        if (connection == null) {
            synchronized (connectionMonitor) {
                // Let's try to load Android SQLite driver
                try {
                    DriverManager.registerDriver((Driver) Class.forName("org.sqldroid.SQLDroidDriver").newInstance());
                } catch (Exception e) {
                    System.err.println("Failed to register SQLDroid database driver. We're not on droid?");
                }

                new File(MethodsObject.getWorkdir(), "databases").mkdir();
                connection = DriverManager.getConnection("jdbc:sqlite:" + MethodsObject.getWorkdir() + "/databases/sample.db");

                // Check & update db
                int desiredVersion = getCurrentDatabaseVersion();
                Statement stmt = connection.createStatement();
                int ownVersion = 0;

                try {
                    ResultSet rs = stmt.executeQuery("SELECT value FROM meta WHERE key = 'version'");
                    if (rs.next()) {
                        ownVersion = rs.getInt(1);
                    }
                } catch (SQLException ignored) {

                }

                if (ownVersion < desiredVersion) {
                    onDatabaseUpdate(connection, ownVersion);

                    if (ownVersion == 0) {
                        stmt.execute("CREATE TABLE meta (key varchar(20) NOT NULL UNIQUE," +
                                " value varchar(20) NOT NULL," +
                                " PRIMARY KEY(key)" +
                                ");");
                    }

                    stmt.execute("replace into meta(key, value) values ('version'," + desiredVersion + ")");
                }

                stmt.close();
            }
        }
        return connection;
    }

    /**
     * Override me
     */
    public int getCurrentDatabaseVersion() {
        return 1;
    }

    /**
     * Override me
     *
     * @param connection use this!
     * @param from       current database version which was specified in `getCurrentDatabaseVersion()`
     *                   of previous adapter release
     */
    public void onDatabaseUpdate(Connection connection, int from) throws SQLException {

    }

    public void closeDb() throws SQLException {
        synchronized (connectionMonitor) {
            connection.close();
            connection = null;
        }
    }

    /*
     * Generated api methods
     */
    /* Generator: methods */

}
