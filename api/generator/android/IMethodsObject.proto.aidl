package ucc.api;

/* Generator: additional imports */

/* Generator: parcelables */
interface IMethodsObject {

    /*
     * Host api
     */
     void addCallback(IEvents callback);
     void removeCallback(IEvents callback);

/* Generator: methods */

}