package ucc.adapter;

import android.app.Activity;
import android.os.Handler;
import ucc.accounting.Accounting;

public class AddAccountActivity extends AccountingBoundActivity {

    @Override
    void onConnected(Accounting accounting) {
        setContentView(R.layout.account_irc);

        findViewById(R.id.btn_cancel).setOnClickListener(view -> {
            finishActivity(Activity.RESULT_CANCELED);
            finish();
        });
        findViewById(R.id.btn_ok).setOnClickListener(view -> {
            // TODO: pass args
            new Thread() {
                @Override
                public void run() {
                    System.out.println("Test account added");
                    new Handler(getMainLooper()).post(() -> {
                        setResult(Activity.RESULT_OK);
                        finish();
                    });
                }
            }.start();
        });
    }
}
