package ucc.adapter;

import ucc.accounting.Accounting;

public class AddAccountActivity extends AccountingBoundActivity {

    @Override
    void onConnected(Accounting accounting) {
        CompatDC.setupDiscord(this, accounting);
    }
}
