package ucc.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.RemoteException;
import android.webkit.*;
import android.widget.Toast;
import ucc.accounting.Accounting;

/**
 * WIP dc accounting
 * <p>
 * TODO: exclude this class for all adapters except DC (via proguard)
 * </p>
 */
public class CompatDC {

    private static final String JS_WORKAROUND_URL = "https://gitlab.com/iria_somobu/ucc/-/blob/master/doc/android.md#uncaught-syntaxerror";

    public static void setupDiscord(Activity ctx, Accounting adapter) {

        String userAgent = "Mozilla/5.0 (Android 7.1.2; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0";

        WebViewClient wvc = new WebViewClient() {

            private AlertDialog finalBlocker = null;

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if ("https://discord.com/app".equals(request.getUrl().toString())) {
                    view.getHandler().post(() -> {
                        // Block user interaction till we find Authorization header
                        finalBlocker = new AlertDialog.Builder(ctx)
                                .setMessage("Login success. Extracting token.\n\n" +
                                        "It'll take some time.")
                                .setCancelable(false)
                                .show();
                    });
                }

                view.loadUrl(request.getUrl().toString(), request.getRequestHeaders());
                return true;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(final WebView view, WebResourceRequest request) {

                if (request.getRequestHeaders() != null) {
                    for (String s : request.getRequestHeaders().keySet()) {

                        if (s.equalsIgnoreCase("authorization")) {
                            String header = request.getRequestHeaders().get(s);
                            if (header == null || header.equals("undefined")) continue;

                            view.getHandler().post(() -> {
                                if (finalBlocker != null) finalBlocker.cancel();
                                view.stopLoading();
                                view.destroy();

                                new Thread() {
                                    @Override
                                    public void run() {
                                        try {
                                            adapter.addAccount(new String[]{header});
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        }

                                        view.getHandler().post(() -> {
                                            ctx.setResult(Activity.RESULT_OK);
                                            ctx.finish();
                                        });
                                    }
                                }.start();
                            });
                        }
                    }
                }

                return super.shouldInterceptRequest(view, request);
            }
        };

        WebView view = setupWebView(ctx, wvc);

        view.getSettings().setUserAgentString(userAgent);
        view.setBackgroundColor(Color.parseColor("#36393e"));

        ctx.setContentView(view);

        view.loadUrl("https://discord.com/login");
        showLoadingDialog(ctx);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private static WebView setupWebView(Activity ctx, WebViewClient wvc) {
        CookieManager.getInstance().removeAllCookies(null);
        WebStorage.getInstance().deleteAllData();

        WebView view = new WebView(ctx);

        WebSettings settings = view.getSettings();
        settings.setAppCacheEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportMultipleWindows(true);

        view.setWebViewClient(wvc);

        view.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {

                if (consoleMessage.message().contains("Uncaught SyntaxError")) {
                    view.getHandler().post(() -> showJsWorkaroundDialog(ctx));
                }

                return super.onConsoleMessage(consoleMessage);
            }
        });

        return view;
    }

    private static void showLoadingDialog(Activity ctx) {
        new AlertDialog.Builder(ctx)
                .setMessage("Here we'll try to extract token from Discord's login webpage. Loading that Electron page " +
                        "may take a while.\n\n" +
                        "Note that 3rd party Discord client usage may result in account ban. On the other hand, " +
                        "I'm (Iria Somobu) using this app since 2019 and so far so good.")
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }

    private static void showJsWorkaroundDialog(Activity ctx) {
        new AlertDialog.Builder(ctx)
                .setTitle("Uncaught SyntaxError")
                .setMessage("Looks like current WebView implementation is unable to process Discord's javascript properly.\n\n" +
                        "This type of error was seen on several devices with older Android versions like 5.1 or 7.1.2.\n\n" +
                        "Press 'Help' button to get list of possible solutions.")
                .setCancelable(false)
                .setNeutralButton("Help", (dialogInterface, i) -> {

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(JS_WORKAROUND_URL));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    try {
                        ctx.startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(ctx, "Unable to open link in browser!", Toast.LENGTH_SHORT).show();
                    }

                    ctx.finish();

                })
                .setPositiveButton("Ok", (dialogInterface, i) -> ctx.finish())
                .show();
    }
}
