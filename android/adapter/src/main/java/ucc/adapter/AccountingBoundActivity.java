package ucc.adapter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import ucc.accounting.Accounting;
import ucc.accounting.IAccounting;

public abstract class AccountingBoundActivity extends Activity {

    private ServiceConnection connection;
    private Accounting adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();

        Intent intent = new Intent();
        intent.setClassName(getPackageName(), AccountingService.class.getCanonicalName());
        connection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                adapter = (Accounting) IAccounting.Stub.asInterface(iBinder);

                onConnected(adapter);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    abstract void onConnected(Accounting accounting);

    @Override
    protected void onDestroy() {
        unbindService(connection);
        super.onDestroy();
    }
}
