package ucc.adapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import ucc.accounting.Accounting;

public class AccountingService extends Service {

    private IBinder binder = null;

    public AccountingService() {
        // Unfortunately, we have to do some workaround here
        Accounting.setWorkdir("/data/data/" + BuildConfig.APPLICATION_ID);

        try {
            binder = (IBinder) Class.forName(BuildConfig.ACCOUNTING_SERVICE).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

}
