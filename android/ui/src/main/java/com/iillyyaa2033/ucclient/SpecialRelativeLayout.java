package com.iillyyaa2033.ucclient;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import com.somobu.ucc.R;

public class SpecialRelativeLayout extends RelativeLayout {

    private float menuWidth; // Как широко может отодвинуться панель
    private float margin; // Отступ нужен для того, чтобы скрыть тенечку

    private float minMoveWidth = 100; // Как сильно юзер должен свайпнуть чтобы панель открылась/закрылась
    private float openTriggerWidth = 200; // Как далеко от края экрана юзер может начать свайп

    private static final float animSpeed = 1.5f; // Скорость анимации

    private Mode mode = Mode.CLOSED;

    private boolean allowed = false;
    private float dX;

    private OnDrawerState listener = null;

    public SpecialRelativeLayout(Context context) {
        super(context);
        init(context);
    }

    public SpecialRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        menuWidth = context.getResources().getDimension(R.dimen.drawer_width);
        margin = context.getResources().getDimension(R.dimen.drawer_margin);
        minMoveWidth = context.getResources().getDimension(R.dimen.drawer_root_min_move);
        openTriggerWidth = context.getResources().getDimension(R.dimen.drawer_root_trigger);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                if (mode == Mode.CLOSED) {
                    if (event.getX() > openTriggerWidth) {
                        allowed = false;
                        return super.dispatchTouchEvent(event);
                    }
                }

                allowed = true;

                dX = getX() - event.getRawX();
                break;

            case MotionEvent.ACTION_MOVE:
                if (!allowed) return super.dispatchTouchEvent(event);

                float x = event.getRawX() + dX;
                if (x > menuWidth - margin) x = menuWidth - margin;
                if (x < -margin) x = -margin;

                animate()
                        .x(x)
                        .setDuration(0)
                        .start();
                break;

            case MotionEvent.ACTION_UP:
                if (!allowed) return super.dispatchTouchEvent(event);

                float finalX = -margin;
                if (mode == Mode.CLOSED) {
                    if (getX() > minMoveWidth) {
                        finalX = menuWidth - margin;
                        mode = Mode.OPENED;
                        if (listener != null) listener.onDrawerOpened();
                    }
                } else {
                    if (getX() < menuWidth - minMoveWidth) {
                        finalX = -margin;
                        mode = Mode.CLOSED;
                        if (listener != null) listener.onDrawerClosed();
                    } else {
                        finalX = menuWidth - margin;
                    }
                }

                long duration = (long) (Math.abs(getX() - finalX) * animSpeed);

                animate()
                        .x(finalX)
                        .setDuration(duration)
                        .start();

                break;
            default:
                return super.dispatchTouchEvent(event);
        }

        return true;
    }

    public void closeDrawer() {
        if (mode == Mode.OPENED) {
            long duration = (long) (Math.abs(getX() - (-margin)) * animSpeed);

            animate()
                    .x(-margin)
                    .setDuration(duration)
                    .start();
            mode = Mode.CLOSED;
            if (listener != null) listener.onDrawerClosed();
        }
    }

    public void setDrawerStateListener(OnDrawerState listener) {
        this.listener = listener;
    }

    enum Mode {
        CLOSED,
        OPENED
    }

    public interface OnDrawerState {
        void onDrawerOpened();

        void onDrawerClosed();
    }
}
