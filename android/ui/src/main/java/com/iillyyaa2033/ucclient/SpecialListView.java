package com.iillyyaa2033.ucclient;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class SpecialListView extends ListView {

    /**
     * https://medium.com/audhil/loading-list-item-at-top-of-listview-without-losing-scroll-position-51f1210335da
     */
    private boolean blockLayoutChildren;

    public SpecialListView(Context context) {
        super(context);
    }

    public SpecialListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpecialListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SpecialListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setBlockLayoutChildren(boolean blockLayoutChildren) {
        this.blockLayoutChildren = blockLayoutChildren;
    }

    @Override
    protected void layoutChildren() {
        if (!blockLayoutChildren) {
            super.layoutChildren();
        }
    }
}
