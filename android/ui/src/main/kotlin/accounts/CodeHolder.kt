package com.somobu.ucc.accounts

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Handler
import android.os.IBinder
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import ucc.accounting.IAccounting

object CodeHolder {

    private val gson: Gson = GsonBuilder().create()

    fun fetchAccountItems(ctx: Context): ArrayList<AccountItem> {
        val items = arrayListOf<AccountItem>()

        val installedAdapters = listInstalledAdapters(ctx)

        val accounts = fetchAccounts(ctx, installedAdapters)
        if (accounts.isNotEmpty()) {
            items.add(AccountItem(ItemMode.DIVIDER, "Accounts"))
            items.addAll(accounts)
        }

        val adapters = installedAdapters.map { adapter ->
            AccountItem(
                    ItemMode.ADD_ACCOUNT, "Add account on " + adapter.name, adapterIcon(ctx, adapter.packageName),
                    adapter.packageName, null, adapter
            )
        }

        if (adapters.isNotEmpty()) {
            items.add(AccountItem(ItemMode.DIVIDER, "Installed adapters"))
            items.addAll(adapters)
        }

        val suggestions = getAdapterSuggestionsList(ctx, installedAdapters)
        if (suggestions.isNotEmpty()) {
            items.add(AccountItem(ItemMode.DIVIDER, "Suggested adapters"))
            items.addAll(suggestions)
        }

        return items
    }

    fun fetchAccounts(ctx: Context): List<AccountItem> = fetchAccounts(ctx, listInstalledAdapters(ctx))

    fun deleteAccount(ctx: Context, item: AccountItem) {
        val service = connectService(ctx, item.service!!)
        service.service.deleteAccount(item.accountId)
        ctx.unbindService(service.connection)
    }

    private fun fetchAccounts(ctx: Context, adapters: List<AccountingServiceInfo>): List<AccountItem> {
        return adapters.flatMap { adapter -> getAccountsOnAdapter(ctx, adapter) }
    }

    private fun listInstalledAdapters(ctx: Context): List<AccountingServiceInfo> {
        val infos = ctx.packageManager.getInstalledPackages(PackageManager.GET_META_DATA)
        val result = arrayListOf<AccountingServiceInfo>()

        for (pkg in infos) {
            val apiVersion = pkg.applicationInfo?.metaData?.getInt("ucc.api.version", -1) ?: -1
            if (apiVersion < 0) continue

            // TODO: compare with our api version

            val appname = ctx.packageManager.getApplicationLabel(pkg.applicationInfo).toString()
            result.add(AccountingServiceInfo(pkg.packageName, "ucc.adapter.AccountingService", appname))
        }

        return result
    }

    private fun connectService(ctx: Context, info: AccountingServiceInfo): ConnectedService {
        val lock = Object()
        val h = Handler(ctx.mainLooper)

        var connection: ServiceConnection? = null
        var adapter: IAccounting? = null

        h.post {
            val intent = Intent()
            intent.setClassName(info.packageName, info.className)
            connection = object : ServiceConnection {

                override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
                    adapter = IAccounting.Stub.asInterface(iBinder)
                    synchronized(lock) { lock.notifyAll() }
                }

                override fun onServiceDisconnected(componentName: ComponentName) {
                    synchronized(lock) { lock.notifyAll() }
                }

            }
            ctx.bindService(intent, connection!!, Context.BIND_AUTO_CREATE)
        }

        synchronized(lock) {
            try {
                lock.wait((5 * 1000).toLong())
                // TODO: we may not connect within given timeout
            } catch (ignored: InterruptedException) {

            }
        }

        return ConnectedService(connection!!, adapter!!)
    }

    private fun getAccountsOnAdapter(ctx: Context, adapter: AccountingServiceInfo): List<AccountItem> {
        val service = connectService(ctx, adapter)

        val items = service.service.listAccounts().map { src ->
            AccountItem(ItemMode.ACCOUNT_INFO, src.name, src.avatar, adapter.name, src.id, adapter)
        }

        ctx.unbindService(service.connection)

        return items
    }

    private fun adapterIcon(ctx: Context, packageName: String): String {
        val name = "$packageName.png"
        for (item in ctx.assets.list("adapters")!!) if (name == item) return "assets://adapters/$name"
        return "assets://adapters/adapter_unknown.png"
    }

    private fun getAdapterSuggestionsList(ctx: Context, installed: List<AccountingServiceInfo>): List<AccountItem> {
        val json = ctx.assets.open("adapters/suggested.json").bufferedReader().use { it.readText() }
        val items = gson.fromJson(json, Array<SuggestedAdapter>::class.java)

        return items.mapNotNull { item ->
            var ignore = false

            for (info in installed) {
                if (item.packageName == info.packageName) {
                    ignore = true
                    break
                }
            }

            var rz: AccountItem? = null
            if (!ignore) {
                rz = AccountItem(ItemMode.INSTALL_ADAPTER, "Install ${item.name}", adapterIcon(ctx, item.packageName))
            }

            rz
        }
    }

}