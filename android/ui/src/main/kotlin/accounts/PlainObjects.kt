package com.somobu.ucc.accounts

import android.content.ServiceConnection
import ucc.accounting.IAccounting

class ConnectedService(val connection: ServiceConnection, val service: IAccounting)

class AccountingServiceInfo(val packageName: String, val className: String, val name: String)

class SuggestedAdapter(val name: String, val packageName: String)

enum class ItemMode { DIVIDER, ADD_ACCOUNT, ACCOUNT_INFO, INSTALL_ADAPTER }

class AccountItem(
        val mode: ItemMode,
        val title: String,
        val icon: String?,
        val subtitle: String?,
        val accountId: String?,
        val service: AccountingServiceInfo?
) {

    constructor(mode: ItemMode, title: String) : this(mode, title, null)

    constructor(mode: ItemMode, title: String, icon: String?) : this(mode, title, icon, null)

    constructor(mode: ItemMode, title: String, icon: String?, service: AccountingServiceInfo?) : this(mode, title, icon, null, null, service)

}