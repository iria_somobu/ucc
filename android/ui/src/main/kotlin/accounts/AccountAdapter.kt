package com.somobu.ucc.accounts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener
import com.somobu.ucc.R
import com.somobu.ucc.ShitCollector

class AccountAdapter(ctx: Context) : ArrayAdapter<AccountItem>(ctx, R.layout.item_account) {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val loader: ImageLoader = ImageLoader.getInstance()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = getItem(position)!!
        val root: View

        if (item.mode == ItemMode.DIVIDER) {
            root = inflater.inflate(R.layout.item_acc_divider, parent, false)
            root.findViewById<TextView>(R.id.title).text = item.title
        } else {
            root = inflater.inflate(R.layout.item_account, parent, false)
            root.findViewById<TextView>(R.id.title).text = item.title

            if (item.subtitle == null || item.subtitle.isEmpty()) {
                root.findViewById<View>(R.id.subtitle).visibility = View.GONE
            } else {
                val t = root.findViewById<TextView>(R.id.subtitle)
                t.visibility = View.VISIBLE
                t.text = item.subtitle
            }

            if (item.icon != null) {

                val listener: SimpleImageLoadingListener = object : SimpleImageLoadingListener() {
                    override fun onLoadingFailed(imageUri: String, view: View, failReason: FailReason) {
                        super.onLoadingFailed(imageUri, view, failReason)
                        ShitCollector.feed("Unable to load image", failReason.cause)
                    }
                }

                val avatar = root.findViewById<ImageView>(R.id.avatar)
                loader.displayImage(item.icon, avatar, listener)
            }
        }

        return root
    }
}