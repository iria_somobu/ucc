package com.somobu.ucc.accounts

import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import com.somobu.ucc.R
import com.somobu.ucc.ShitCollector
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AccountsActivity : Activity() {

    private lateinit var adapter: AccountAdapter
    private lateinit var packageBr: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts)

        adapter = AccountAdapter(this)
        val accountsList = findViewById<ListView>(R.id.adapters_list)
        accountsList.adapter = adapter
        accountsList.onItemClickListener = OnItemClickListener { _: AdapterView<*>?, _: View?, index: Int, _: Long ->
            val item: AccountItem = adapter.getItem(index)!!
            when (item.mode) {
                ItemMode.DIVIDER -> {}
                ItemMode.ADD_ACCOUNT -> addAccount(item)
                ItemMode.ACCOUNT_INFO -> deleteAccount(item)
                ItemMode.INSTALL_ADAPTER -> installAdapter(item)
            }
        }

        // Register package broadcast
        packageBr = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                reloadList()
            }
        }

        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_PACKAGE_ADDED)
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED)
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED)
        filter.addDataScheme("package")
        registerReceiver(packageBr, filter)
    }

    override fun onResume() {
        super.onResume()
        reloadList()
    }

    override fun onDestroy() {
        unregisterReceiver(packageBr)
        super.onDestroy()
    }

    private fun reloadList() {
        GlobalScope.launch {
            val list = CodeHolder.fetchAccountItems(this@AccountsActivity)

            runOnUiThread {
                adapter.clear()
                adapter.addAll(list)
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun addAccount(item: AccountItem) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.setClassName(item.service!!.packageName, "ucc.adapter.AddAccountActivity")
        startActivity(intent)
    }

    private fun deleteAccount(item: AccountItem) {
        AlertDialog.Builder(this)
                .setMessage("Do you really want to delete account ${item.title} @${item.subtitle}?")
                .setNegativeButton("No", null)
                .setPositiveButton("Yes") { _: DialogInterface, _: Int ->
                    GlobalScope.launch {
                        CodeHolder.deleteAccount(this@AccountsActivity, item)
                        runOnUiThread {
                            reloadList()
                        }
                    }
                }
                .show()
    }

    private fun installAdapter(item: AccountItem) {
        val uri = Uri.parse("https://gitlab.com/iria_somobu/ucc")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        try {
            startActivity(intent)
        } catch (e: Exception) {
            ShitCollector.feed("Unable to find browser activity", e)
        }
    }
}