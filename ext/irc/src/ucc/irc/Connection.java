package ucc.irc;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;
import ucc.accounting.Account;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Connection to a single IRC server as a single client
 */
public class Connection {

    private Account account = new Account();

    private PircBot bot = new PircBot() {

    };

    public Connection() {
        account.id = "test";
        account.name = "Test IRC account";

        bot.setAutoNickChange(false);
        try {
            bot.setEncoding("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        bot.changeNick("");
        try {
            bot.connect("", 0, "");
        } catch (IOException | IrcException e) {
            e.printStackTrace();
        }
    }

    public Account account() {
        return account;
    }

}
