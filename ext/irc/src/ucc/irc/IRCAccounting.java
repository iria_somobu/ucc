package ucc.irc;

import ucc.accounting.Account;
import ucc.accounting.Accounting;
import ucc.accounting.AdapterInfo;

import java.util.ArrayList;
import java.util.List;

public class IRCAccounting extends Accounting {

    private static final String ADAPTER_ID = "irc";

    private List<Connection> connections = new ArrayList<>();

    @Override
    public AdapterInfo info() {
        AdapterInfo info = new AdapterInfo();
        info.id = ADAPTER_ID;
        info.name = "IRC";
        return info;
    }

    @Override
    public Account[] listAccounts() {
        ArrayList<Account> accounts = new ArrayList<>();
        for (Connection cn : connections) accounts.add(cn.account());
        return accounts.toArray(new Account[0]);
    }

}
