# UCC

Modular chat client for Android that aims to support a several social networks. Now almost non-functional, but I'm
working on it.

## Feature matrix

As you can see in the table below, nothing works properly yet.

| Network    | Texting | Voice |
|------------|:-------:|:-----:|
| Discord    |         |       |
| IRC        |         |  N/A  |
| Telegram   |         |       |

## How to build

Basically, `gradle gradle :android:ui:build` for UI and `gradle :android:adapter:build` for adapters.

But first:

- set up Android SDK and gradle OR open project in Android Studio;
- generate api sources by calling `./api/generate.sh`;


